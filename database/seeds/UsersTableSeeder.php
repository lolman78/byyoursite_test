<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'George',
                'email' => 'georgeanti99@gmail.com',
                'role_id' => '1',
                'password' => bcrypt('Woid1999'),
                'created_at' => now()
            ],
            [
                'name' => 'Chris',
                'email' => 'Chrisanti2001@gmail.com',
                'role_id' => '1',
                'password' => bcrypt('ChrisAnti'),
                'created_at' => now()
            ],
            [
                'name' => 'Steve',
                'email' => 'Steve21@gmail.com',
                'role_id' => '2',
                'password' => bcrypt('Gebruiker21'),
                'created_at' => now()
            ],
        ]);
    }
}
