<?php

use Illuminate\Database\Seeder;

class VacaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vacatures')->insert([
            [
                'titel' => 'Test',
                'functie_omschrijving' => 'Dit is een test Vacature.',
                'sollicitatie_link' => 'https://nl.indeed.com/vacature-bekijken?from=app-tracker-saved-appcard&hl=nl&jk=ac1cf011f474aa61&tk=1g65g620fhupn800',
                'bedrijf' => 'Testland',
                'locatie' => 'Groningen',
                'user_id' => '1',
                'created_at' => now()
            ],
            [
                'titel' => 'Test2',
                'functie_omschrijving' => 'Dit is een 2de test Vacature.',
                'bedrijf' => 'Testland',
                'locatie' => 'Eelde',
                'sollicitatie_link' => 'https://nl.indeed.com/vacature-bekijken?from=app-tracker-saved-appcard&hl=nl&jk=ac1cf011f474aa61&tk=1g65g620fhupn800',
                'user_id' => '2',
                'created_at' => now()
            ],
            [
                'titel' => 'Test3',
                'functie_omschrijving' => 'Dit is een 3de test Vacature.',
                'sollicitatie_link' => 'https://nl.indeed.com/vacature-bekijken?from=app-tracker-saved-appcard&hl=nl&jk=ac1cf011f474aa61&tk=1g65g620fhupn800',
                'bedrijf' => 'Testland',
                'locatie' => 'Assen',
                'user_id' => '3',
                'created_at' => now()
            ],
        ]);
    }
}
