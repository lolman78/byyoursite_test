@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Nieuwe Vacature aanmaken') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('vacature.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="titel"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Titel:') }}
                                </label>

                                <div class="col-md-6">
                                    <input id="titel" type="text"
                                        class="form-control @error('titel') is-invalid @enderror" name="titel"
                                        value="{{ old('titel') }}" autocomplete="titel" autofocus>

                                    @error('titel')
                                        <span class="invalid-feedback" role="titel">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="functie_omschrijving"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Functie Omschrijving:') }}</label>

                                <div class="col-md-6">
                                    <textarea id="functie_omschrijving" class="form-control
                                        @error('functie_omschrijving') is-invalid @enderror"
                                            name="functie_omschrijving" autocomplete="functie_omschrijving" rows="6">
                                        {{ old('functie_omschrijving') }}
                                    </textarea>

                                    @error('functie_omschrijving')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bedrijf"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Bedrijf:') }}
                                </label>

                                <div class="col-md-6">
                                    <input id="bedrijf" type="text"
                                        class="form-control @error('bedrijf') is-invalid @enderror" name="bedrijf"
                                        value="{{ old('bedrijf') }}" autocomplete="bedrijf" autofocus>

                                    @error('bedrijf')
                                        <span class="invalid-feedback" role="bedrijf">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="locatie"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Locatie:') }}
                                </label>

                                <div class="col-md-6">
                                    <input id="locatie" type="text"
                                        class="form-control @error('locatie') is-invalid @enderror" name="locatie"
                                        value="{{ old('locatie') }}" autocomplete="locatie" autofocus>

                                    @error('locatie')
                                        <span class="invalid-feedback" role="locatie">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sollicitatie_link"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Solicitatie link:') }}
                                </label>

                                <div class="col-md-6">
                                    <input id="sollicitatie_link" type="text"
                                        class="form-control @error('sollicitatie_link') is-invalid @enderror" name="sollicitatie_link"
                                        value="{{ old('sollicitatie_link') }}" autocomplete="sollicitatie_link" autofocus>

                                    @error('sollicitatie_link')
                                        <span class="invalid-feedback" role="sollicitatie_link">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Schuld aanmaken') }}
                                    </button>
                                    <a href="javascript:history.back()" style="margin-left: 20px">Terug</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#functie_omschrijving'), {
                removePlugins: ['bulletedList', 'numberedList'],
                toolbar: ['Heading', 'bold', 'italic', 'Link', 'blockQuote']
            })
            .then(functie_omschrijving => {
                console.log(functie_omschrijving);

                const data = functie_omschrijving.getData();
                console.log(data);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
