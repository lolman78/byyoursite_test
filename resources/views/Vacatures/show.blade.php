@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="margin-bottom: 20px">
                    <div class="card-header"></div>
                    <div class="card-body">
                        <h1>{{ $vacature->titel }}</h1>
                        <b>{{ $vacature->bedrijf.' - '.$vacature->locatie }}</b>
                        <p>
                            {!! $vacature->functie_omschrijving !!}
                        </p>      
                    </div>

<!--                deze link zou je kunnen uitbreiden dat het je door stuurt naar een solicitatie pagina
                    of naar de website van het bedrijf zodat je daar kan Soliciteren
                    afhankelijk van hoe je de site wil uitbreiden -->
                    <div class="row">
                        <p style="margin-left: 20px;">
                            <div class="col-md-2" style="margin-bottom: 20px;">
                                <a href={{$vacature->sollicitatie_link}}
                                    class="btn btn-primary">Soliciteren
                                </a>
                            </div>                         

                            <div class="col-md-2" style="margin-top: 7px">
                                <a class="text-secondary"
                                    href="javascript:history.back()">Terug</a>
                                </a>
                            </div>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
