@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header text-center">
                        Profiel van {{ $user->name }}
                    </div>

                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">

                        <div class="col-md-12 col-form-label text-md-center">
                            @if($user->profilepicture == null)
                                <img height="200px" width="200px" src="/img/profile/default/default.png" alt="">
                            @else
                                <img height="200px" width="200px" src="/img/profile/{{ $user->profilepicture }}" alt="">
                            @endif
                        </div>
                    </div>

                    @if($user->profilepicture)
                        <div class="mx-auto">
                            <a href="{{ route('user.destroyProfilePicture', $user) }}"
                                class="btn btn-secondary">Profiel foto verwijderen
                            </a>
                        </div>
                    @endif

                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link text-center" id="v-pills-team-tab" href="{{ route('user.profile', $user) }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false">Mijn profiel
                        </a>
                        <a class="nav-link text-center" id="v-pills-team-tab" href="{{ route('user.myDebts', $user) }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="true"><b>Mijn schulden</b>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h1 style="display: inline-block;">Mijn schulden</h1>

                        <div style="display: inline-block; float: right;">
                            <a class="btn btn-secondary"
                            href="{{ route('debt.create') }}">{{ ('Nieuwe schuld maken') }}</a>
                        </div>
                    </div>
                    <table class="rwd-table">
                        <tbody>
                        <tr>
                            <th style="padding-left: 10px; padding-top: 10px">Prijs</th>
                            <th style="padding-left: 10px; padding-top: 10px">Schuldige</th>
                            <th style="padding-top: 10px">Status</th>
                            <th style="padding-top: 10px">Created at</th>
                            <th style="padding-top: 10px">Updated at</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($debts as $debt)
                            <tr>
                                <td data-th="Title" style="padding-left: 10px">
                                    <a href="{{ route('debt.show', $debt) }}">
                                        {{ $debt->euros }}.{{ $debt->cents}}
                                    </a>
                                </td>
                                <td data-th="Title" style="padding-left: 10px">
                                    {{ $debt->debt_for }}
                                </td>
                                <td data-th="Title">
                                    {{ $debt->status->name }}
                                </td>
                                <td data-th="Created_at">
                                    @if($debt->created_at)
                                        {{ date_format($debt->created_at, 'd-m-Y') }}
                                    @endif
                                </td>
                                <td data-th="Updated_at">
                                    @if($debt->updated_at)
                                        {{ date_format($debt->updated_at, 'd-m-Y') }}
                                    @endif
                                </td>
                                <td data-th="Acties">
                                    <form method="POST" action="{{ route('debt.destroy', $debt) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                            onclick="return confirm('weet je zeker dat je deze schuld van {{$debt->debt_for}} wilt verwijderen?')"
                                            class="btn text-secondary">Verwijderen
                                        </button>
                                    </form>
                                </td>
                                <td data-th="Acties">
                                    <a href="{{ route('debt.edit', $debt) }}"
                                       class="btn btn-primary">Bewerken
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
