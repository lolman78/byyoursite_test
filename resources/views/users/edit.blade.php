@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Gebuiker bewerken') }}</div>

                    <div class="card-body">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist" style="margin-bottom: 20px;">
                            <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab"
                               href="#nav-profile" role="tab" aria-controls="nav-profile"
                               aria-selected="true">Profiel
                            </a>

                            <a class="nav-item nav-link" id="nav-password-tab" data-toggle="tab"
                               href="#nav-password" role="tab" aria-controls="nav-password"
                               aria-selected="false">Wachtwoord
                            </a>
                        </div>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-profile" role="tabpanel"
                                 aria-labelledby="nav-profile-tab">

                                <form method="POST" action="{{ route('user.update', $user) }}">
                                    @method('PUT')
                                    @csrf

                                    <div class="form-group row">
                                        <label for="name"
                                            class="col-md-4 col-form-label text-md-right">{{ __('Naam*') }}
                                        </label>

                                        <div class="col-md-6">
                                            <input id="name" type="text"
                                                class="form-control @error('name') is-invalid @enderror" name="name"
                                                value="{{ old('name', $user->name) }}" autocomplete="name" autofocus>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email"
                                            class="col-md-4 col-form-label text-md-right">{{ __('Email*') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="text"
                                                class="form-control @error('email') is-invalid @enderror"
                                                name="email"
                                                value="{{ old('email', $user->email) }}" autocomplete="email"
                                                autofocus disabled>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="role_id" class="col-md-4 col-form-label text-md-right">
                                            Rol
                                        </label>

                                        <div class="col-md-6">
                                            <select id="role_id"
                                                class="form-control @error('role_id') is-invalid @enderror"
                                                name="role_id" value="{{ old('role_id', $user->role_id) }}" required
                                                autocomplete="role_id" autofocus>
                                                <option value="{{$user->role_id}}">{{ $user->role->name }}</option>
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}">{{ $role->name }}</option>
                                                @endforeach
                                            </select>

                                            @error('role_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Gebruiker bewerken') }}
                                            </button>

                                            <a class="text-secondary"
                                               href="{{ route('admin.users') }}">Terug
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane fade" id="nav-password" role="tabpanel"
                                 aria-labelledby="nav-password-tab">

                                <form method="POST" action="{{ route('user.updatePassword', $user) }}">
                                    @method('PUT')
                                    @csrf

                                    <div class="form-group row">
                                        <label for="password"
                                            class="col-md-4 col-form-label text-md-right">{{ __('Wachtwoord*') }}
                                        </label>

                                        <div class="col-md-6">
                                            <input id="password" type="password"
                                                class="form-control @error('password') is-invalid @enderror"
                                                name="password" required autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-1">
                                            <img src="/img/app/hint.png" alt="" height="20px" width="20px" 
                                            class="mt-2"
                                            title=
                                            "Jouw wachtwoord moet minimaal uit 8 tekens bestaan met:
1 hoofdletter 
1 kleine letter 
1 nummer 
1 speciale teken">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm"
                                            class="col-md-4 col-form-label text-md-right">{{ __('Wachtwoord bevestigen*') }}
                                        </label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control"
                                            name="password_confirmation" required autocomplete="new-password">
                                        </div>

                                        <div class="col-md-1">
                                            <img src="/img/app/hint.png" alt="" height="20px" width="20px" 
                                            class="mt-2" title="Confirm password">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Wachtwoord bewerken') }}
                                            </button>

                                            <a class="text-secondary"
                                               href="{{ route('admin.users') }}">Terug
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
