@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(Auth::User() == null)
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        {{ ('Hallo Bezoeker') }}<br>
                        {{ ('Welkom bij de Vacature Bank, Maak een account aan en bekijk al onze vacatures') }}<br>
                        {{ ('En vind je volgende baan HIER!') }}
                    </div>
                </div>
            </div>    
        @endIf

        @if(!Auth::User() == null)
            <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h1 style="display: inline-block;">Vacatures</h1>
                        </div>
                        <table class="rwd-table table-striped">
                            <tbody>
                                <tr>
                                    <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">Functie</th>
                                    <th style="padding-top: 10px; padding-bottom: 10px">Bedrijf</th>
                                    <th style="padding-top: 10px padding-bottom: 10px">Locatie</th>
                                    <th style="padding-top: 10px padding-bottom: 10px">Aangemaakt op</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                @foreach($vacatures as $vacature)
                                    <tr>
                                        <td data-th="Titel" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                            <a href="{{ route('vacature.show', $vacature->id) }}">
                                                {{ $vacature->titel }}
                                            </a>
                                        </td>
                                        <td data-th="Bedrijf">
                                            {{ $vacature->bedrijf }}
                                        </td>
                                        <td data-th="Locatie">
                                            {{ $vacature->locatie }}
                                        </td>
                                        <td data-th="Created_at">
                                            @if($vacature->created_at)
                                                {{ date_format($vacature->created_at, 'd-m-Y') }}
                                            @endif
                                        </td>
                                        @can('isAdmin', Auth::user())
                                            <td data-th="Acties">
                                                <form method="POST" action="{{ route('vacature.destroy', $vacature) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit"
                                                        onclick="return confirm('weet je zeker dat je deze schuld van {{$vacature->titel}} wilt verwijderen?')"
                                                        class="btn text-secondary">Verwijderen
                                                    </button>
                                                </form>
                                            </td>
                                        @endcan
                                        @can('isAdmin', Auth::user())
                                            <td data-th="Acties">
                                                <a href="{{ route('vacature.edit', $vacature) }}"
                                                class="btn btn-primary">Bewerken
                                                </a>
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endIf
    </div>
</div>
@endsection
