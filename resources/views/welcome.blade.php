@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    {{ ('Hallo Bezoeker') }}<br>
                    {{ ('Welkom bij de Vacature Bank, Maak een account aan en bekijk al onze vacatures') }}<br>
                    {{ ('En vind je volgende baan hier.') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
