@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row mb-4">
            <div class="col-md-3 p-0">
                <div class="card">
                    <div class="card-header">
                        Admin menu
                    </div>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.users') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false">Gebruikers</a>
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.vacatures') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="true"><b>Vacatures</b></a>
                        <a class="nav-link" id="v-pills-user-tab" href="{{ route('admin.roles') }}"
                           role="tab" aria-controls="v-pills-user" aria-selected="false">Rollen</a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h1 style="display: inline-block;">Schulden</h1>

                        <div class="dropdown" style="display: inline-block; float: right;">
                            <button class="btn btn-secondary  float-right" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Acties
                            </button>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item btn float-right"
                                    href="{{ route('user.create') }}">{{ ('Nieuwe Gebruiker maken') }}
                                </a>
                                <a class="dropdown-item btn float-right"
                                   href="{{ route('vacature.create') }}">{{ ('Nieuwe Vacature maken') }}
                                </a>
                                <a class="dropdown-item btn float-right"
                                   href="{{ route('role.create') }}">{{ ('Nieuwe Rol maken') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <table class="rwd-table table-striped">
                        <tbody>
                            <tr>
                                <th style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">Functie</th>
                                <th style="padding-top: 10px; padding-bottom: 10px;">Bedrijf</th>
                                <th style="padding-top: 10px; padding-bottom: 10px;">Locatie</th>
                                <th style="padding-top: 10px; padding-bottom: 10px;">Created at</th>
                                <th style="padding-top: 10px; padding-bottom: 10px;">Updated at</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach($vacatures as $vacature)
                                <tr>
                                    <td data-th="Titel" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <a href="{{ route('vacature.show', $vacature->id) }}">
                                            {{ $vacature->titel }}
                                        </a>
                                    </td>
                                    <td data-th="Bedrijf">
                                        {{ $vacature->bedrijf }}
                                    </td>
                                    <td data-th="Locatie">
                                        {{ $vacature->locatie }}
                                    </td>
                                    <td data-th="Created_at">
                                        @if($vacature->created_at)
                                            {{ date_format($vacature->created_at, 'd-m-Y') }}
                                        @endif
                                    </td>
                                    <td data-th="Updated_at">
                                        @if($vacature->updated_at)
                                            {{ date_format($vacature->updated_at, 'd-m-Y') }}
                                        @endif
                                    </td>
                                    <td data-th="Acties">
                                        <form method="POST" action="{{ route('vacature.destroy', $vacature) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit"
                                                onclick="return confirm('weet je zeker dat je deze schuld van {{$vacature->titel}} wilt verwijderen?')"
                                                class="btn text-secondary">Verwijderen
                                            </button>
                                        </form>
                                    </td>
                                    <td data-th="Acties">
                                        <a href="{{ route('vacature.edit', $vacature) }}"
                                            class="btn btn-primary">Bewerken
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
