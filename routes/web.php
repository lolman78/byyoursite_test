<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// Admin
Route::get('/admin/users', 'AdminController@users')->name('admin.users');
Route::get('/admin/vacatures', 'AdminController@vacatures')->name('admin.vacatures');
Route::get('/admin/roles', 'AdminController@roles')->name('admin.roles');

// Users
Route::get('/user/index', 'UserController@index')->name('user.index');
Route::get('/user/create', 'UserController@create')->name('user.create');
Route::post('/user/store', 'UserController@store')->name('user.store');
Route::get('/user/{user}/show', 'UserController@show')->name('user.show');
Route::get('/user/{user}/edit', 'UserController@edit')->name('user.edit');
Route::put('/user/{user}/update', 'UserController@update')->name('user.update');
Route::delete('/user/{user}/delete', 'UserController@destroy')->name('user.destroy');
Route::put('/user/{user}/updatePassword', 'UserController@updatePassword')->name('user.updatePassword');

// Vacatures
Route::get('/vacature/index', 'VacatureController@index')->name('vacature.index');
Route::get('/vacature/create', 'VacatureController@create')->name('vacature.create');
Route::post('/vacature/store', 'VacatureController@store')->name('vacature.store');
Route::get('/vacature/{vacature}/show', 'VacatureController@show')->name('vacature.show');
Route::get('/vacature/{vacature}/edit', 'VacatureController@edit')->name('vacature.edit');
Route::put('/vacature/{vacature}/update', 'VacatureController@update')->name('vacature.update');
Route::delete('vacature/{vacature}/delete', 'VacatureController@destroy')->name('vacature.destroy');

// Roles
Route::get('/role/index', 'RoleController@index')->name('role.index');
Route::get('/role/create', 'RoleController@create')->name('role.create');
Route::post('/role/store', 'RoleController@store')->name('role.store');
Route::get('/role/{role}/show', 'RoleController@show')->name('role.show');
Route::get('/role/{role}/edit', 'RoleController@edit')->name('role.edit');
Route::put('/role/{role}/update', 'RoleController@update')->name('role.update');
Route::delete('role/{role}/delete', 'RoleController@destroy')->name('role.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
