<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            request()->validate(
            [
                'name' => 'required | string | max:255', 
                'email' => 'required | string | email | max:255 | unique:users,email', // geldig, uniek
                'role_id' => 'required | min:1',
                'password' => ['required' , 'string', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/'], 
                // minimaal 8 tekens, 1 hoofdletter, minimaal 1 cijfer, minimaal 1 bijzonder teken
            ])
        ];
    }
}
