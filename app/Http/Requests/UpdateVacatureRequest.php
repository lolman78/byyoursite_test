<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateVacatureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        // haal het idee van de ingelogde user op
        $user_id = Auth::User()->id;

        request()->merge(
        [
            'user_id' => $user_id,
        ]);
        
        // dd(request()->functie_omschrijving);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            request()->validate([
                'titel' => 'required|max:255',
                'functie_omschrijving' => 'nullable|string|max:1000',
                'bedrijf' => 'required|max:255',
                'locatie' => 'required|max:255',
                'sollicitatie_link' => 'required|max:255',
                'user_id' => 'required',
            ])
        ];
    }
}
