<?php

namespace App\Http\Requests;

use Auth;
use App\Vacature;
use Illuminate\Foundation\Http\FormRequest;

class StoreVacatureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        // haal het idee van de ingelogde user op
        $user_id = Auth::User()->id;

        // zet de functie_omschrijving in een variabele
        $functie_omschrijving = Request()->get("functie_omschrijving");

        // haal alle <p> tags eruit
        $functie_omschrijving = str_replace("<p>", "", $functie_omschrijving);

        // verander de </p> tags met <br>
        $functie_omschrijving = str_replace("</p>", "<br>", $functie_omschrijving);

        request()->merge(
        [
            'user_id' => $user_id,
            'functie_omschrijving' => $functie_omschrijving,
        ]);
        
        // dd(request()->functie_omschrijving);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Vacature::create(request()->validate([
                'titel' => 'required|max:255',
                'functie_omschrijving' => 'nullable|string|max:1000',
                'bedrijf' => 'required|max:255',
                'locatie' => 'required|max:255',
                'sollicitatie_link' => 'required|max:255',
                'user_id' => 'required',
            ]))
        ];
    }
}
