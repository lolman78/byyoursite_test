<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if(request()->role_id == null)
        {
            request()->merge(
            [
               'role_id' => Auth::User()->role_id,
               'password' => bcrypt(request()->password),
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            [
                request()->validate(
                [
                  'name' => 'required | max:255',
                  'role_id' => 'required | min:1',
                ])
            ]
        ];
    }
}
