<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Vacature;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UpdateUserPasswordRequest;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('createAndStore', User::class);
        $roles = Role::orderBy('created_at')->get();

        return view('users.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $this->authorize('createAndStore', User::class);

        // Als de gebruiker is gevalideerd maak de user aan
        $user = User::create([
            'name' => request()->name,
            'email' => request()->email,
            'role_id' => 2,
            'password' => Hash::make(request()->password),
        ]);

        $user->save();

        return redirect()->route('admin.users')->with('success', 'Gebruiker aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('editAndUpdate', $user);
        $roles = Role::orderBy('created_at')->whereNotIn('id', [$user->role_id])->get();
      
        return view('users.edit', ['roles' => $roles, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('editAndUpdate', $user);
        $user->update(request()->all());

        return back()->with('success', 'Gebruiker bewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('destroy', $user);

        $user->delete();

        return back()->with('success', 'Gebruiker verwijderd');
    }

    public function updatePassword(UpdateUserPasswordRequest $request, User $user)
    {
        $this->authorize('updatePassword', $user);
        
        request()->merge(
            [
                'password' => bcrypt(request()->password),
            ]);

        $user->update(request()->all());

        return back()->with('success', 'Je wachtwoord is bewerkt!');
    }
}
