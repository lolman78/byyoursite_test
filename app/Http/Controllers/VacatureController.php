<?php

namespace App\Http\Controllers;

use App\User;
use App\Vacature;
use App\Status;
use Illuminate\Http\Request;
use App\Http\Requests\StoreVacatureRequest;
use App\Http\Requests\UpdateVacatureRequest;

class VacatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('createAndStore', Vacature::class);

        return view('vacatures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVacatureRequest $request)
    {
        $this->authorize('createAndStore', Vacature::class);
        $vacature = Vacature::latest()->first();
        
        return redirect()->route('vacature.show', compact('vacature'))->with('success', 'Nieuwe vacature aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vacature  $vacature
     * @return \Illuminate\Http\Response
     */
    public function show(Vacature $vacature)
    {
        $this->authorize('show', $vacature);

        return view('vacatures.show', compact('vacature'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vacature  $vacature
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacature $vacature)
    {
        $this->authorize('editAndUpdate', $vacature);

        return view('vacatures.edit', compact('vacature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vacature  $vacature
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVacatureRequest $request, Vacature $vacature)
    {
        $this->authorize('editAndUpdate', $vacature);
        $vacature->update(request()->all());

        // verander dit in de toekomst naar debts.show
        return redirect()->route('vacature.show', compact('vacature'))->with('success', 'Vacature is succesvol bijgewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vacature  $vacature
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacature $vacature)
    {
        $this->authorize('destroy', $debt);

        $debt->delete();

        return redirect()->route('javascript:history.back()', compact('user'))->with('success', 'Vacature verwijderd');
    }
}
