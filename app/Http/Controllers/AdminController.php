<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Vacature;
use App\Role;

class AdminController extends Controller
{
    public function users()
    {
        $this->authorize('isAdmin', User::class);
        $users = User::orderBy('name')->get();

        return view('admin.users', compact('users'));
    }

    public function vacatures()
    {
        $this->authorize('isAdmin', User::class);
        $vacatures = Vacature::orderBy('created_at')->get();

        return view('admin.vacatures', ['vacatures' => $vacatures]);
    }

    public function roles()
    {
        $this->authorize('isAdmin', User::class);
        $roles = Role::orderBy('name')->get();

        return view('admin.roles', ['roles' => $roles]);
    }
}
