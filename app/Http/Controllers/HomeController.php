<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Vacature;
use App\Role;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::User() === null){

            return view('home');
        }

        $vacatures = Vacature::orderBy('created_at')->get();

        // Het lukte mij de xml objecten op te halen hier.
        // Maar had toen problemen met de show pagina in de tabel.
        // $xmlString = file_get_contents(public_path('xml/vacatures.xml'));
        // $xmlObject = simplexml_load_string($xmlString);                   
        // $json = json_encode($xmlObject);
        // $vacatures = json_decode($json, true); 
        // dd($vacatures);

        return view('home', compact('vacatures'));
    }
}
