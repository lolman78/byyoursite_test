<?php

namespace App\Policies;

use App\User;
use App\Vacature;
use Illuminate\Auth\Access\HandlesAuthorization;

class VacaturePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function loggedIn(User $user)
    {
      // misschien zou het beter zijn als dit gewoon kijkt of $user true 
      // Als $user een object terug geeft (ofterwijl als de user is ingelogd) mogen ze door naar de pagina
      return $user; // kijk of de $user true is zo ja, dan is de user ingelogd
    }

    public function isAdmin(User $user)
    {
      return $user->role->name == 'Admin';
    }

    public function createAndStore(User $user)
    {
      return ($this->isAdmin($user));
    }

    public function show(User $user, Vacature $vacature)
    {
      return ($this->loggedIn($user));
    }
 
    public function editAndUpdate(User $user, Vacature $vacature)
    {
      return ($this->isAdmin($user));
    }

    public function destroy(User $user, Vacature $vacature)
    {
      return ($this->isAdmin($user));
    }
}
