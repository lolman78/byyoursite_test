<?php

namespace App\Policies;

use Auth;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function loggedIn(User $user)
    {
      // misschien zou het beter zijn als dit gewoon kijkt of $user true 
      // Als $user een object terug geeft (ofterwijl als de user is ingelogd) mogen ze door naar de pagina
      return $user; // kijk of de $user true is zo ja, dan is de user ingelogd
    }

    public function isCorrectUser(User $user)
    {
      // Als de id van de ingelogde user gelijk is aan de id in de url?
      // dan mag de user naar de profiel pagina met de id in url
      return $user->id === request()->user->id;
    }

    public function isAdmin(User $user)
    {
      // Kijk of de user de role admin of niet heeft
      return $user->role->name == 'Admin';
    }

    public function createAndStore(User $user)
    {
      // Als de user een admin is.
      // Dan mag de user het profiel aanpassen.
      return ($this->isAdmin($user));
    }

    public function editAndUpdate(User $user)
    {
      // Als de user id hetzelfde is als de ingelogde user of de user is een admin.
      // Dan mag de user het profiel aanpassen.
      return ($this->isCorrectUser($user)) || ($this->isAdmin($user));
    }

    public function destroy(User $user)
    {
      // Als de user een admin is.
      // Dan mag de user het profiel aanpassen.
      return ($this->isAdmin($user));
    }

    public function Profile(User $user)
    {
      // Als de id van de user gelijk is aan de user id van de profiel pagina waar je heen wil
      // Of de role van de ingelogde user is admin
      // Mag de user naar de profiel pagina 
      return ($this->isCorrectUser($user)) || ($this->isAdmin($user));
    }

    public function updatePassword(User $user)
    {
      // Als de user is ingelogd
      // En als de user id gelijk is aan de ingelogde user of de ingelogde user is een admin.
      // mogen ze het wachtwoord veranderen
      return (($this->loggedIn($user)) && ($this->isCorrectUser($user) || $this->isAdmin($user)));
    }
}
