<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacature extends Model
{
    protected $fillable = [
        'titel', 'functie_omschrijving','bedrijf', 'locatie', 'sollicitatie_link', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
